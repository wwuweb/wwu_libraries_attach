<?php

namespace Drupal\wwu_libraries_attach\Plugin\Filter;

use Drupal\Component\Utility\Html;
use Drupal\filter\Plugin\FilterBase;
use Drupal\filter\FilterProcessResult;

/**
 * Adds the Featherlight library when the data-featherlight attribute is
 * present.
 *
 * @Filter(
 *   id = "filter_libraries_attach",
 *   title = @Translation("Attach component libraries"),
 *   description = @Translation("Load JS/CSS library when certain class or attributes are present."),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_MARKUP_LANGUAGE
 * )
 */
class FilterLibrariesAttach extends FilterBase {

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode) {
    $result = new FilterProcessResult($text);
    $html = Html::load($text);
    $xpath = new \DOMXPath($html);

    $switcher = $xpath->query('//*[contains(@class,"content-switcher")]');
    $links = $xpath->query('//a[@target="blank" or @target="_blank"]');
    $bg_video = $xpath->query('//*[contains(@class,"bg-video-container")]');
    $table = $xpath->query('//table');

    $libraries = [];
    if (count($switcher)) {
      $libraries[] = 'ashlar/switcher';
    }

    if (count($links)) {
      $libraries[] = 'ashlar/external-links-a11y';
    }

    if (count($bg_video)) {
      $libraries[] = 'ashlar/bg-video';
    }

    if (count($table)) {
      $libraries[] = 'ashlar/table-a11y';
    }

    $result->setAttachments(['library' => $libraries]);

    return $result;
  }

}
